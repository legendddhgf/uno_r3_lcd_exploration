#include <avr/io.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>

int main(void){

  sei();

  // set bit 7 of port D to be an output
  DDRD |= 0xA0;

  for (;;) {
    _delay_ms(500);
    // toggle bit 7 of portd
    PORTD ^= 0xA0;
  }

  return 0;
}
